use anyhow::Result;
use chrono::{Duration, NaiveDate, NaiveDateTime, Timelike};
use log::debug;
use std::collections::{HashMap, HashSet};
use std::time::Instant;

#[derive(Clone, Copy, Debug, PartialEq)]
enum SleepState {
    Asleep,
    Awake,
    Stable,
}

#[derive(Clone, Debug)]
struct GuardStats {
    workdays: HashSet<NaiveDate>,
    sleep_minutes: Vec<u16>,
}

impl Default for GuardStats {
    fn default() -> Self {
        GuardStats {
            workdays: HashSet::new(),
            sleep_minutes: vec![0; 60],
        }
    }
}

pub fn day_4(input: &str) -> Result<()> {
    let start = Instant::now();

    let mut sleep_tracker: HashMap<NaiveDate, Vec<SleepState>> = HashMap::new();
    let mut guard_dates: HashMap<String, GuardStats> = HashMap::new();

    for schedule_entry in input.lines() {
        let datetime = match NaiveDateTime::parse_from_str(
            schedule_entry.get(1..=16).unwrap(),
            "%Y-%m-%d %H:%M",
        )? {
            dt if dt.hour() == 23 => dt + Duration::hours(1),
            dt => dt,
        };
        let day_entry = sleep_tracker
            .entry(datetime.date())
            .or_insert(vec![SleepState::Stable; 60]);
        match schedule_entry.get(19..).unwrap() {
            "falls asleep" => {
                day_entry[datetime.minute() as usize] = SleepState::Asleep;
            }
            "wakes up" => {
                day_entry[datetime.minute() as usize] = SleepState::Awake;
            }
            note => {
                let guard_id = note.split_whitespace().skip(1).take(1).collect();
                let guard_stats = guard_dates.entry(guard_id).or_default();
                guard_stats.workdays.insert(datetime.date());
            }
        };
    }

    let mut worst_guard_sleep_total = 0;
    let mut worst_guard = "O.G.".to_owned();

    for guard in guard_dates.iter_mut() {
        for day in guard.1.workdays.iter() {
            let mut sleeping = 0;
            for (guard_minute, day_minute) in guard
                .1
                .sleep_minutes
                .iter_mut()
                .zip(sleep_tracker.get(day).unwrap())
            {
                sleeping = match day_minute {
                    SleepState::Asleep => 1,
                    SleepState::Awake => 0,
                    SleepState::Stable => sleeping,
                };

                *guard_minute += sleeping;
            }
        }

        let guard_sleep_total: u16 = guard.1.sleep_minutes.iter().sum();
        if guard_sleep_total > worst_guard_sleep_total {
            debug!("new worst guard: {} => {}", worst_guard, guard.0);
            debug!(
                "new sleep total: {} => {}",
                worst_guard_sleep_total, guard_sleep_total
            );
            worst_guard = guard.0.to_owned();
            worst_guard_sleep_total = guard_sleep_total;
        }
    }

    // DEBUG: graph awake/asleep for a given guard
    if log::max_level() >= log::LevelFilter::Debug {
        let gid = "#499";
        debug!("XXXX-XX-XX  000000000011111111112222222222333333333344444444445555555555");
        debug!("            012345678901234567890123456789012345678901234567890123456789");
        let guard = guard_dates.get(gid).unwrap();
        for day in guard.workdays.iter() {
            let mut chart_line = format!("{}  ", day);
            let mut sleeping = 0;

            for day_minute in sleep_tracker.get(day).unwrap() {
                sleeping = match day_minute {
                    SleepState::Asleep => {
                        chart_line.push_str("X");
                        1
                    }
                    SleepState::Awake => {
                        chart_line.push_str("_");
                        0
                    }
                    SleepState::Stable => {
                        if sleeping == 0 {
                            chart_line.push_str("_");
                        } else if sleeping == 1 {
                            chart_line.push_str("X");
                        }
                        sleeping
                    }
                };
            }
            debug!("{}", chart_line);
        }
    }

    let worst_guard_id = worst_guard.trim_start_matches('#').parse::<u32>()?;
    let mut worst_guard_minute = 0;
    let mut worst_guard_minute_count = 0;
    let mut guard_asleep_most_consistently_id = 0;
    let mut guard_asleep_most_consistently_minute = 0;
    let mut guard_asleep_most_consistently_minute_count = 0;

    // parse out worst sleepytime offenders and stats
    for guard in guard_dates {
        let (most_consistent_minute, most_consistent_minute_count) = guard
            .1
            .sleep_minutes
            .iter()
            .enumerate()
            .max_by_key(|&(_, count)| count)
            .unwrap();

        // store values for part 1 answer, looking at the overall worst guard
        if guard.0 == worst_guard {
            worst_guard_minute = most_consistent_minute;
            worst_guard_minute_count = *most_consistent_minute_count;
        }
        // store values for part 2 answer, looking for the guard most asleep at consistent times
        if *most_consistent_minute_count > guard_asleep_most_consistently_minute_count {
            guard_asleep_most_consistently_id = guard.0.trim_start_matches('#').parse::<u32>()?;
            guard_asleep_most_consistently_minute = most_consistent_minute;
            guard_asleep_most_consistently_minute_count = *most_consistent_minute_count;
        }
    }
    debug!("worst guard id: {}", worst_guard_id);
    debug!("worst minute on average: {}", worst_guard_minute);
    debug!("times asleep on worst minute: {}", worst_guard_minute_count);

    let worst_guard_calc = worst_guard_id * worst_guard_minute as u32;
    let most_consistent_guard_calc =
        guard_asleep_most_consistently_id * guard_asleep_most_consistently_minute as u32;

    let end = Instant::now();
    crate::utils::log_day_results(
        "4",
        &worst_guard_calc,
        &most_consistent_guard_calc,
        end.duration_since(start),
    );
    Ok(())
}
