use anyhow::Result;
use std::collections::HashSet;
use std::time::Instant;

pub fn day_3(input: &str) -> Result<()> {
    let start = Instant::now();

    let mut fabric = [[(0u16, 0u8); 1000]; 1000]; // try Vec once complete
    let mut isolated_claim_ids: HashSet<u16> = HashSet::new();

    for claim in input.lines() {
        let mut overlap = false;
        let claim_split: Vec<_> = claim.split_whitespace().collect();
        let left_top_split: Vec<_> = claim_split[2].trim_end_matches(":").split(",").collect();
        let wide_tall_split: Vec<_> = claim_split[3].split("x").collect();
        let (id, left, top, wide, tall): (u16, usize, usize, usize, usize) = (
            claim_split[0].trim_start_matches('#').parse()?,
            left_top_split[0].parse()?,
            left_top_split[1].parse()?,
            wide_tall_split[0].parse()?,
            wide_tall_split[1].parse()?,
        );
        for fabric_row in fabric.iter_mut().skip(top).take(tall) {
            for fabric_region in fabric_row.iter_mut().skip(left).take(wide) {
                match fabric_region.0 {
                    0 => fabric_region.0 = id,
                    _ => {
                        overlap = true;
                        isolated_claim_ids.remove(&fabric_region.0);
                    }
                }
                fabric_region.1 += 1;
            }
        }
        if !overlap {
            isolated_claim_ids.insert(id);
        }
    }

    let overlapping: u32 = fabric.iter().fold(0, |acc, fabric_row| {
        acc + fabric_row.iter().fold(0, |acc_row, fabric_region| {
            acc_row
                + match fabric_region.1 {
                    0..=1 => 0,
                    _ => 1,
                }
        })
    });

    assert!(isolated_claim_ids.len() == 1);
    let isolated_claim: u16 = isolated_claim_ids.drain().sum();

    let end = Instant::now();
    crate::utils::log_day_results(
        "3",
        &overlapping,
        &isolated_claim,
        end.duration_since(start),
    );
    Ok(())
}
