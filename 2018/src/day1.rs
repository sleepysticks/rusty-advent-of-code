use anyhow::Result;
use std::collections::HashSet;
use std::time::Instant;

pub fn day_1(input: &str) -> Result<()> {
    let start = Instant::now();

    let parsed_input: Vec<i32> = input.lines().map(|l| l.parse().unwrap()).collect();
    let frequency_sum: i32 = parsed_input.iter().sum();

    let mut frequencies = HashSet::new();
    let mut frequency = 0;
    parsed_input.iter().cycle().find(|freq_mod| {
        frequency += *freq_mod;
        !frequencies.insert(frequency)
    });

    let end = Instant::now();
    crate::utils::log_day_results("1", &frequency_sum, &frequency, end.duration_since(start));
    Ok(())
}
