use anyhow::Result;
use std::collections::HashMap;
use std::time::Instant;

pub fn day_2(input: &str) -> Result<()> {
    let start = Instant::now();

    let mut char_count_matches: HashMap<char, u32> = HashMap::with_capacity(26);
    let (doubles, triples) = input.lines().fold((0, 0), |(doubles, triples), line| {
        char_count_matches.clear();
        line.chars()
            .for_each(|letter| *char_count_matches.entry(letter).or_default() += 1);
        (
            doubles + u32::from(char_count_matches.values().any(|&count| count == 2)),
            triples + u32::from(char_count_matches.values().any(|&count| count == 3)),
        )
    });
    let rudimentary_checksum = doubles * triples;

    let mut diff_index: i32 = -1;
    let mut matching_letters: String = "".to_owned();
    for line1 in input.lines() {
        for line2 in input.lines() {
            if line1 == line2 {
                continue;
            }
            for ((index, char1), char2) in line1.char_indices().zip(line2.chars()) {
                if char1 != char2 {
                    if diff_index != -1 {
                        diff_index = -1;
                        break;
                    }
                    diff_index = index as i32;
                }
            }
            if diff_index != -1 {
                matching_letters = line1.to_owned();
                matching_letters.remove(diff_index as usize);
                break;
            }
        }
        if diff_index != -1 {
            break;
        }
    }

    let end = Instant::now();
    crate::utils::log_day_results(
        "2",
        &rudimentary_checksum,
        &matching_letters,
        end.duration_since(start),
    );
    Ok(())
}
