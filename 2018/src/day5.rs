use anyhow::Result;
use std::time::Instant;

pub fn day_5(input: &str) -> Result<()> {
    let start = Instant::now();

    // TBD

    let end = Instant::now();
    crate::utils::log_day_results(
        "5",
        &0,
        &0,
        end.duration_since(start),
    );
    Ok(())
}
