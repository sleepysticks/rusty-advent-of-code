mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod input;
mod utils;

use anyhow::Result;

fn main() -> Result<()> {
    setup_logging(log::LevelFilter::Info).expect("failed to initialize logging.");

    day1::day_1(input::AOC_DAY1_INPUT)?;
    day2::day_2(input::AOC_DAY2_INPUT)?;
    day3::day_3(input::AOC_DAY3_INPUT)?;
    day4::day_4(input::AOC_DAY4_INPUT)?;
    day5::day_5(input::AOC_DAY5_INPUT)?;
    Ok(())
}

fn setup_logging(verbosity: log::LevelFilter) -> Result<(), fern::InitError> {
    let base_config = fern::Dispatch::new().level(verbosity);

    let stdout_config = fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "[{}][{}][{}] {}",
                chrono::Local::now().format("%H:%M"),
                record.target(),
                record.level(),
                message
            ))
        })
        .chain(std::io::stdout());

    base_config.chain(stdout_config).apply()?;

    Ok(())
}
