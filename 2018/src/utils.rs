use log::info;
use std::fmt::Display;

pub fn log_day_results(
    day: &str,
    part_1_result: &dyn Display,
    part_2_result: &dyn Display,
    day_runtime: std::time::Duration,
) {
    info!("Day {} Part 1: {}", day, part_1_result);
    info!("Day {} Part 2: {}", day, part_2_result);
    info!(
        "Day {} Total Runtime: {} ms\n",
        day,
        day_runtime.as_millis(),
    );
}
